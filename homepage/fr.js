{
	"Translated By": "Yann D. - gitlab.com/yaningo - github.com/yaningo",
	"Gitter &mdash; Where developers come to talk.": "Gitter &mdash; L'espace de discussion pour développeurs",
	"Integrations": "Intégrations",
	"Pricing": "Tarifs",
	"Apps": "Applications",
	"Sign In to Start Talking": "Connectez-vous pour commencer une discussion",
	"login": "connexion",
	"API": "API",
	"Blog": "Blog",
	"Twitter": "Twitter",
	"Support": "Assistance",

	"Where communities thrive": "L'incubateur de communautés",
	"Gitter is a chat and networking platform that helps to manage, grow and connect communities through messaging, content and discovery.": "Gitter est une plate-forme de discussion et de résautage qui vous aide à gérer, développer et relier des communautés en associant messagerie, contenu et découverte",
	"Create your own community": "Créez votre propre communauté",
	"Explore other communities": "Découvrez d'autres communautés",
	"By signing up you agree to our <a %s>Terms and Conditions</a>": "En vous inscrivant, vous acceptez nos <a %s>Conditions Générales d'Utilisation</a>",
	"Free without limits": "Gratuit et illimité",
	"Enjoy free public and private communities with unlimited people, message history and integrations.": "Offrez-vous le luxe de créer des communautés publiques et privées avec un nombre de personnes, un historique de messagerie et un nombre d'intégrations illimités.",
	"Simple to create": "Création simple",
	"Simply create your community and start talking - no need to set up any invitation services.": "lI vous suffit tout simplement de créer votre communauté et de commencer une discussion - vous n'avez aucun service d'invitation à configurer.",
	"Quick to grow": "Expansion rapide",
	"Grow your community in no time with our easy sharing tools.": "Grace à nos outils de partage simplifié, votre communauté s'aggrandit en un rien de temps.",
	"Out in the open": "Ouverte sur le monde",
	"Let your community be discovered! With Gitter, everyone can find your community in our directory and via search engines.": "Votre communauté n'attent que d'être découverte ! Avec Gitter, tout le monde a la possibilité de trouver votre communauté, que ce soit dans notre répertoire ou via les moteurs de recherche.",

	"Trusted by": "Ils nous font confiance",
	"Some of our communities": "Quelques-unes de nos communautés",
	"Create your own": "Créez la votre",
	"Explore rooms": "Explorez les espaces de discussion",
	"Explore": "Explorez",
	"GitterHQ": "GitterHQ",
	"Let’s talk about Gitter!": "À propos de Gitter !",

	"Designed for community collaboration": "Conçu pour la collaboration entre communautés",
	"Gitter is designed to make community messaging, collaboration and discovery as smooth and simple as possible. You can easily create, organise and grow your communities, inviting others to join just in one click.": "Gitter est conçu pour rendre l'échange de messages, la collaboration et la découverte entre communautés aussi fluides et simples que possible. Vous pouvez facilement créer, organiser et faire grandir votre communauté, en invitant d'autres utilisateurs, en un seul clic.",
	"We also provide integrations with GitHub, Trello, Jenkins, Travis CI, Heroku, Sentry, BitBucket, HuBoard, Logentries, Pagerduty & Sprintly. We accept custom webhooks, have an <a %s>open source repository</a> for integrations as well as a <a %s>flexible API</a>.": "Nous fournissons également des intégrations pour GitHub, Trello, Jenkins, Travis CI, Heroku, Sentry, BitBucket, HuBoard, Logentries, Pagerduty & Sprintly. Nous acceptons les <i>webhooks</i> personnalisés, nous avons un dépôt <i>open source</i> pour les intégrations, ainsi qu'une <a %s><i>API</i> flexible</a>.",

	"Loved by our users": "Nos utilisateurs l'adorent",
	"Gitter is nothing without the people using it day in and day out.": "Gitter ne serait rien sans les personnes qui l'utilisent quotidiennement",
	"We asked a few people what Gitter means to them, this is what they had to say. If you love Gitter, Tweet us <a %s>%s</a> and we'll update these quotes every month.": "Nous avons demandé à quelques personnes ce qu'elles pensent de Gitter ; voici ce qu'elles nous ont répondu. Si vous aimez Gitter, envoyez-nous un <i>tweet</i> et nous mettrons à jour ces témoignages tous les mois.",

	"Pricing plans are coming early 2017. Join the Earlybird programme now and get a 75%% discount.": "",
	"Prices may be subject to VAT. By signing up you agree to our <a %s>Terms and Conditions</a>.": "Les taris peuvent varier en fonction de la TVA. En vous inscrivant, vous acceptez nos <a %s>Conditions Générales d'Utilisation</a>.",
	"Free": "Gratuit",
	"Forever": "Pour toujours",
	"<b>Unlimited</b> chat history": "Historique de conversation illimité",
	"<b>Unlimited</b> integrations": "Nombre d'intégration illimité",
	"<b>Unlimited</b> public rooms": "Nombre d'espaces publics de discussion illimité",
	"<b>Unlimited</b> private rooms": "Nombre d'espaces privés de discussion illimité",
	"Limited to 25 users per private room": "25 utilisateurs maximum par espace privé de discussion",
	"<b>Get started</b> for free": "Commencez dès maintenant, gratuitement",
	"Premium Plans": "Plans Premium",
	"starting from": "à partir de",
	"per month": "par mois",
	"Community <b>insights</b> & analytics": "Statistiques et analyses",
	"Community <b>engagement</b> & marketing tools": "Outils de marketing et de gestion de l'expérience utilisateur",
	"<b>Customisable look</b> of the community rooms": "Apparence personnalisable des espaces de discussion",
	"<b>PR & promotional</b> tools": "Outils de <b>publicité</b> et de <b>relations publiques</b>",
	"Access to <b>community managers</b>": "Accès aux <b>community managers</b>",
	"<b>Join</b> earlybird programme": "Inscrivez-vous au programme d'accès anticipé",

	"Get Gitter now!": "Obtenez Gitter dès maintenant !",
	"Gitter is available in all modern browsers as well as apps for desktops and mobile phones.": "Gitter est compatible avec tous les navigateurs récents ; des applications pour ordinateurs de bureau et téléphones portables sont également dsiponibles",
	"Mac": "Mac",
	"Windows": "Windows",
	"Linux 32": "Linux 32",
	"Linux 64": "Linux 64",
	"iPhone": "iPhone",
	"Android": "Android",
	"Open Source": "Open Source",
	"Our efforts are focused on the <a %s>webapp</a> which is the backbone of the mobile/desktop apps but mainly focused on the web experience. There are a number of bugs in these desktop/mobile clients and they spread the Gitter team too thin for us to give them proper attention. The apps are open-source if you want to tackle something particularly annoying to you.": "Nous concentrons nos efforts sur l'<a %s>application web (webapps)</a> qui est le socle des applications pour téléphones portables et ordinateurs de bureau, mais principalement centrés sur l'expérience web. Il y a un certain nombre de bugs dans ces clients pour ordinateurs de bureau/téléphones portables, et l'équipe Gitter n'a pas suffisamment de ressources pour leur accorder l'attention nécessaire. Les applications sont open-source; au cas où vous souhaiteriez résoudre un problème qui vous gêne particuliérement.",
	"desktop": "ordinateurs de bureau",
	"iOS": "iOS",
	"Learn more about <a %s>creating communities</a> and <a %s>creating rooms</a>.": "Informations supplementaires sur la <a %s>création de communautés</a> and <a %s>création d'espaces de discussion</a>.",
	"Mac users on 10.9 or lower, <a %s>download here</a>.": "Utilisateurs de MacOS version 10.9 ou antérieure, <a %s>téléchargez ici</a>.",
	"We also have a sweet little <a %s>IRC bridge</a>.": "Nous avons aussi un <a %s>canal IRC</a> très sympathique."
}
